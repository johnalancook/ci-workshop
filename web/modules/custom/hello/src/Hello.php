<?php

namespace Drupal\hello;

/**
 * Class Hello.
 *
 * @package Drupal\hello
 */
class Hello {

  /**
   * Says hello.
   *
   * @param string $name
   *   The name.
   *
   * @return string
   *   Hello message
   */
  public function sayHello($name) {
    return 'Hello ' . $name;
  }

}
